; (println "\nHello World N Times")
(defn hello_word_n_times [n]
  (dotimes [_ n] (println "Hello World")))
(hello_word_n_times 0)

; (println "\nList Replication")
(defn repeat-list [num lst]
  (mapcat #(repeat num %) lst))
(repeat-list 2 [3 4])

; (println "\nFilter Positions in a List")
(defn f [lst]
  (take-nth 2 (next lst)))
(f (range 5))

; (println "\nReverse a List")
(defn my-reverse [lst]
  (when lst
    (cons (last lst) (my-reverse (butlast lst)))))
(my-reverse (range 5))

; (println "\nSum of Odd Elements")
(defn sum-odd [lst]
  (apply + (filter odd? lst)))
(sum-odd (range 5))

; (println "\nFilter Array")
((fn [delim lst] (filter (partial > delim) lst)) 3 (range 5))

; (println "\nList Length")
(let [l (range 10)]
  (defn my-count [lst] (reduce (fn [acc v] (inc acc)) 0 lst))
  (my-count l))

; (println "\nUpdate List")
((fn [lst] (map #(max % (-' %)) lst)) (map #(- % 5) (range 10)))

; (println "\nEvaluating e^x")

(defn term [x i]
  (if (= 0 i)
    1
    (/ (reduce * (repeat i x)) (reduce * (range 1 (inc i))))))

(defn series [x, n]
  (apply + (map term (repeat n x) (range (inc n)))))

(for [x [20 5 0.5 -0.5]] (format "%.4f" (double (series x 10))))

; (println "\nArea Under Curves and Volume of Revolving a Curve")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; simulate input from STDIN for sample test cases
(defn input [text] (def line (seq (clojure.string/split-lines text))))

(defn print-input [in]
  (when-let [l (in)]
    (do
      (prn l)
      (recur in))))

(defn read-line []
  (when-let [l (first line)]
    (do
      (def line (next line))
      l)))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(input
 "1 2 3 4 5
6 7 8 9 10
1 4")

(defn series [a b xi]
  (reduce + (map #(* %1 (Math/pow xi %2)) a b)))

(defn integral [a b x d]
  (* d (reduce + (map #(series a b %) x))))

(defn volume [a b x d]
  (* d Math/PI (reduce + (map #(Math/pow (series a b %) 2) x))))

(let [a (read-string (str "[" (read-line) "]"))
      b (read-string (str "[" (read-line) "]"))
      n (read-string (str "[" (read-line) "]"))
      d 0.001
      x (range (n 0) (n 1) d)]
  (format "%.1f" (integral a b x d))
  (format "%.1f" (volume a b x d)))

; (println "\nFunctions or Not?")
(input
 "3
2
5 5
5 6
3
1 1
2 2
3 3
4
1 2
2 4
3 6
4 8")

(dotimes [i (read-string (read-line))]
  (def m {})
  (dotimes [j (read-string (read-line))]
    (let [a (read-string (str "[" (read-line) "]"))
          s (get m (a 0))]
      (if (nil? s)
        (def m (assoc m (a 0) #{(a 1)}))
        (def m (assoc m (a 0) (conj s (a 1)))))))
  (if (nil? (some #(> (count %) 1) (vals m)))
    "YES"
    "NO"))

; (println "\nCompute the Perimeter of a Polygon")

(input
 "4
0 0
0 1
1 1
1 0")

(defn distance [a b]
  (Math/sqrt (+ (Math/pow (- (a 0) (b 0)) 2) (Math/pow (- (a 1) (b 1)) 2))))

(let [n (read-string (read-line))
      a (map (fn [i] (read-string (str "[" (read-line) "]"))) (range n))
      b (conj a (last a))]
  (format "%.1f" (reduce + (map distance a b))))

; (println "\nCompute the Area of a Polygon")

(input
 "4
0 0
0 1
1 1
1 0")

(defn xny1-x1yn [v1 vn]
  (let [xny1 (* (vn 0) (v1 1))
        x1yn (* (v1 0) (vn 1))]
    (- xny1 x1yn)))

(defn shoelace-formula [v u]
  (/ (Math/abs (reduce + (map xny1-x1yn v u))) 2.0))

(let [n (read-string (read-line))
      v (map (fn [_] (read-string (str "[" (read-line) "]"))) (range n))
      u (conj v (last v))]
  (format "%.1f" (shoelace-formula v u)))

; (println "\nPentagonal Numbers")
(input
 "5
1
2
3
4
5")

(def m {1 1})

(defn pentagon [n]
  (if (= 1 n)
    1
    (if (contains? m n)
      (get m n)
      (let [ans (+ (pentagon (dec n)) 1 (* 3 (dec n)))]
        (def m (assoc m n ans))
        ans))))

(let [n (read-string (read-line))
      v (map (fn [_] (read-string (read-line))) (range n))]
  (doseq [i v]
    (pentagon i)))

; (println "\nFibonacci")
(input
 "5
0
1
5
10
100")

(defn fib [i]
  (loop [n i
         s [0N 1N]]
    (if (zero? n)
      (first s)
      (recur (dec n) [(second s) (+ (first s) (second s))]))))

(let [n (read-string (read-line))
      v (map (fn [_] (read-string (read-line))) (range n))]
  (doseq [i v]
    (str (fib i))))

; (println "\nFibonacci Numbers")
(input "5")

(let [n (read-string (read-line))]
  (str (fib (dec n))))

; (println "\nPascal's Triangle")
(input "4")

(let [n (read-string (read-line))
      i '(1)]
  (loop [a i]
    ; (apply prn a)
    (when (< (count a) n)
      (recur (concat i (map + a (next a)) i)))))

; (println "\nComputing the GCD")
(input "6 9")

(defn gcd [m n]
  (if (zero? n)
    m
    (recur n (mod m n))))

(let [f (fn [a b] (gcd b a))
      [m n] (map read-string (re-seq #"\d+" (read-line)))]
  (f m  n))

; (println "\nFunctions and Fractals: Sierpinski triangles")
(input "3")

(def text
  "_______________________________1_______________________________
______________________________111______________________________
_____________________________11111_____________________________
____________________________1111111____________________________
___________________________111111111___________________________
__________________________11111111111__________________________
_________________________1111111111111_________________________
________________________111111111111111________________________
_______________________11111111111111111_______________________
______________________1111111111111111111______________________
_____________________111111111111111111111_____________________
____________________11111111111111111111111____________________
___________________1111111111111111111111111___________________
__________________111111111111111111111111111__________________
_________________11111111111111111111111111111_________________
________________1111111111111111111111111111111________________
_______________111111111111111111111111111111111_______________
______________11111111111111111111111111111111111______________
_____________1111111111111111111111111111111111111_____________
____________111111111111111111111111111111111111111____________
___________11111111111111111111111111111111111111111___________
__________1111111111111111111111111111111111111111111__________
_________111111111111111111111111111111111111111111111_________
________11111111111111111111111111111111111111111111111________
_______1111111111111111111111111111111111111111111111111_______
______111111111111111111111111111111111111111111111111111______
_____11111111111111111111111111111111111111111111111111111_____
____1111111111111111111111111111111111111111111111111111111____
___111111111111111111111111111111111111111111111111111111111___
__11111111111111111111111111111111111111111111111111111111111__
_1111111111111111111111111111111111111111111111111111111111111_
111111111111111111111111111111111111111111111111111111111111111")

(defn sierpinski [a r]
  (let [height 32
        c \_
        [x y w h] r
        h2 (/ h 2)]
    (vec (for [i (range height)
               :let [j (- i y)
                     s (get a i)]]
           (if (and (>= j h2) (< j h))
             (str (subs s 0 (+ x j)) (apply str (repeat (- w j j) c)) (subs s (- (+ x w) j)))
             s)))))

(defn regions [r]
  (let [[x y w h] r
        h2 (int (/ h 2))
        w2 (int (/ w 2))
        w4 (int (/ w 4))]
    (list [(+ x w4 1) y w2 h2]
          [x (+ y h2) w2 h2]
          [(+ x w2 1) (+ y h2) w2 h2])))

(defn fractalize [a r]
  (if (nil? r)
    a
    (recur (sierpinski a (first r)) (next r))))

(let [n (read-string (read-line))
      array (clojure.string/split-lines text)]
  (doseq [s (loop [i 0
                   r '([0 0 63 32])
                   a array]
              (if (>= i n)
                a
                (recur (inc i) (mapcat regions r) (fractalize a r))))]
    s))

; (println "\nFunctions and Fractals - Recursive Trees")
; (input "3")

; (defn tree [h]
;   (let [h2 (int (/ h 2))
;         c '(\1)
;         b \_]
;     (concat     (for [i (range h2)]
;                   (apply str (concat (repeat i b) c (repeat (- h 1 i i) b) c (repeat i b))))
;                 (for [i (range h2)]
;                   (apply str (concat (repeat h2 b) c (repeat h2 b)))))))

; (defn forest [width n]
;   (let [w (int (inc (/ width 2 n)))]
;     (prn width n w)
;     (map tree (repeat n w))))

; (doseq [s (forest 63 2)]
;   (prn)
;   (doseq [t s] (prn t)))

(println "\nBreadth First Search: Shortest Reach")
(input
 "2
5 4
1 2
1 3
2 3
2 4
1
3 1
2 3
2")

; Complete the bfs function below.
(defn bfs [n m edges s]
  (let [r (range 1 (inc n))
        e (map set edges)
        adj (zipmap r (for [i r] (disj (reduce into #{} (filter #(% i) e)) i)))]
    (loop [v (list s)
           dist (zipmap r (for [i r] (if (= i s) 0 nil)))]
      (let [v2 (remove dist (reduce into #{} (map adj v)))]
        (if (zero? (count v2))
          (map #(if (nil? %) -1 %) (map dist (remove #(= s %) r)))
          (recur v2 (merge dist (zipmap v2 (repeat (+ 6 (dist (first v))))))))))))

(defn bfs2 [n m edges s]
  (let [r (range 1 (inc n))
        e (map set edges)
        adj (zipmap r (for [i r] (disj (reduce into #{} (filter #(% i) e)) i)))]
    (loop [v (list s)
           dist (zipmap r (for [i r] (if (= i s) 0 nil)))]
      (let [a (first v)
            neighbors (remove dist (adj a))
            q (concat (next v) neighbors)]
        (if (empty? q)
          (map #(if (nil? %) -1 %) (map dist (remove #(= s %) r)))
          (recur q (merge dist (zipmap neighbors (repeat (+ 6 (dist a)))))))))))

(def q (Integer/parseInt (clojure.string/trim (read-line))))
(doseq [q-itr (range q)]
  (def nm (clojure.string/split (read-line) #" "))
  (def n (Integer/parseInt (clojure.string/trim (nth nm 0))))
  (def m (Integer/parseInt (clojure.string/trim (nth nm 1))))
  (def edges [])
  (doseq [_ (range m)]
    (def edges (conj edges (vec (map #(Integer/parseInt %) (clojure.string/split (read-line) #" "))))))
  (def s (Integer/parseInt (clojure.string/trim (read-line))))
  (def result (bfs n m edges s))
  (prn result))
