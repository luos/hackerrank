;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; simulate input from STDIN for sample test cases
(defn input [text] (def line (seq (clojure.string/split-lines text))))

(defn print-input [in]
  (when-let [l (in)]
    (do
      (prn l)
      (recur in))))

(defn read-line []
  (when-let [l (first line)]
    (do
      (def line (next line))
      l)))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(input
"5
1
2
3
4
5")

(def m {1 1})

(defn pentagon [n]
  (if (= 1 n)
    1
    (if (contains? m n)
      (get m n)
      (let [ans (+ (pentagon (dec n)) 1 (* 3 (dec n)))]
        (def m (assoc m n ans))
        ans))))

(let [n (read-string (read-line))
      v (map (fn [_] (read-string (read-line))) (range n))]
  (doseq [i v]
    (prn (pentagon i))))
