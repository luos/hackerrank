/// Breadth First Search: Shortest Reach
/// https://www.hackerrank.com/challenges/bfsshortreach/problem

#include <bits/stdc++.h>

using namespace std;

vector<string> split_string(string);

vector<int> bfs(int n, int m, vector<vector<int>> edges, int s) {
  unordered_multimap<int, int> adjacency;
  unordered_map<int, int> distances;
  for (auto i : edges) {
    adjacency.emplace(i.front(), i.back());
    adjacency.emplace(i.back(), i.front());
  }
  for (int i = 1; i <= n; i++) {
    distances[i] = -1;
  }
  queue<int> vertices;
  vertices.push(s);
  distances[s] = 0;
  while (!vertices.empty()) {
    auto i = vertices.front();
    vertices.pop();
    auto range = adjacency.equal_range(i);
    for (auto it = range.first; it != range.second; ++it) {
      auto j = it->second;
      if (-1 == distances[j]) {
        vertices.push(j);
        distances[j] = distances[i] + 6;
      }
    }
  }
  vector<int> result;
  result.reserve(n - 1);
  for (int i = 1; i <= n; i++) {
    if (i != s) {
      result.push_back(distances[i]);
    }
  }
  return result;
}

// Complete the bfs function below.
vector<int> bfs_clojure(int n, int m, vector<vector<int>> edges, int s) {
  unordered_multimap<int, int> adjacency;
  unordered_map<int, int> distances;
  for (auto i : edges) {
    adjacency.emplace(i.front(), i.back());
    adjacency.emplace(i.back(), i.front());
  }
  for (int i = 1; i <= n; i++) {
    distances[i] = -1;
  }
  queue<int> vertices;
  vertices.push(s);
  distances[s] = 0;
  while (!vertices.empty()) {
    vector<int> v;
    while (!vertices.empty()) {
      v.push_back(vertices.front());
      vertices.pop();
    }
    for (auto i : v) {
      auto range = adjacency.equal_range(i);
      for (auto it = range.first; it != range.second; ++it) {
        auto j = it->second;
        if (-1 == distances[j]) {
          vertices.push(j);
          distances[j] = distances[i] + 6;
        }
      }
    }
  }
  vector<int> result;
  result.reserve(n - 1);
  for (int i = 1; i <= n; i++) {
    if (i != s) {
      result.push_back(distances[i]);
    }
  }
  return result;
}

int main() {
  string input =
      R"(2
4 2
1 2
1 3
1
3 1
2 3
2)";

  stringstream ss(input);
  //  string s;
  //  while (ss >> s) {
  //    cout << s << endl;
  //  }
  auto &cin = ss;

  //  ofstream fout(getenv("OUTPUT_PATH"));
  auto &fout = cout;

  int q;
  cin >> q;
  cin.ignore(numeric_limits<streamsize>::max(), '\n');

  for (int q_itr = 0; q_itr < q; q_itr++) {
    string nm_temp;
    getline(cin, nm_temp);

    vector<string> nm = split_string(nm_temp);

    int n = stoi(nm[0]);

    int m = stoi(nm[1]);

    vector<vector<int>> edges(m);
    for (int i = 0; i < m; i++) {
      edges[i].resize(2);

      for (int j = 0; j < 2; j++) {
        cin >> edges[i][j];
      }

      cin.ignore(numeric_limits<streamsize>::max(), '\n');
    }

    int s;
    cin >> s;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    vector<int> result = bfs(n, m, edges, s);

    for (int i = 0; i < result.size(); i++) {
      fout << result[i];

      if (i != result.size() - 1) {
        fout << " ";
      }
    }

    fout << "\n";
  }

  //  fout.close();

  return 0;
}

vector<string> split_string(string input_string) {
  string::iterator new_end =
      unique(input_string.begin(), input_string.end(),
             [](const char &x, const char &y) { return x == y and x == ' '; });

  input_string.erase(new_end, input_string.end());

  while (input_string[input_string.length() - 1] == ' ') {
    input_string.pop_back();
  }

  vector<string> splits;
  char delimiter = ' ';

  size_t i = 0;
  size_t pos = input_string.find(delimiter);

  while (pos != string::npos) {
    splits.push_back(input_string.substr(i, pos - i));

    i = pos + 1;
    pos = input_string.find(delimiter, i);
  }

  splits.push_back(
      input_string.substr(i, min(pos, input_string.length()) - i + 1));

  return splits;
}
