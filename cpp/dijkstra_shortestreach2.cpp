#include <bits/stdc++.h>
#include <unordered_map>

using namespace std;

vector<string> split_string(string);

inline unsigned long long int key(unsigned long long int a,
                                  unsigned long long int b) {
  return (a << 32) | (b & 0xFFFFFFFF);
}

// Complete the shortestReach function below.
vector<int> shortestReach(int n, vector<vector<int>> edges, int s) {
  unordered_multimap<int, int> adjacency;
  unordered_map<unsigned long long int, int> length;
  unordered_map<int, int> distance;
  vector<int> q;
  auto comp = [&distance](int a, int b) { return distance[a] > distance[b]; };
  auto e2 = edges.size() << 1;
  adjacency.reserve(e2);
  length.reserve(e2);
  distance.reserve(n);
  q.reserve(n);
  auto infinity = numeric_limits<int>::max();
  for (int i = 1; i <= n; i++) {
    distance[i] = infinity;
  }
  distance[s] = 0;

  for (auto const &i : edges) {
    auto k01 = key(i[0], i[1]);
    auto k10 = key(i[1], i[0]);
    if (length.find(k01) == length.end()) {
      length[k01] = i[2];
      adjacency.emplace(i[0], i[1]);
      length[k10] = i[2];
      adjacency.emplace(i[1], i[0]);
    } else {
      if (i[2] < length[k01]) {
        length[k01] = i[2];
        length[k10] = i[2];
      }
    }
  }

  q.push_back(s);
  while (!q.empty()) {
    auto u = q.front();
    pop_heap(q.begin(), q.end(), comp);
    q.pop_back();
    auto r = adjacency.equal_range(u);
    for (auto it = r.first; it != r.second; it++) {
      auto v = it->second;
      auto alt = distance[u] + length[key(u, v)];
      if (alt < distance[v]) {
        auto d = distance[v];
        distance[v] = alt;
        if (d == infinity) {
          q.push_back(v);
          push_heap(q.begin(), q.end(), comp);
        } else {
          make_heap(q.begin(), q.end(), comp);
        }
      }
    }
  }

  vector<int> ans;
  ans.reserve(n - 1);
  for (int i = 1; i <= n; i++) {
    if (i != s) {
      auto t = distance[i];
      ans.push_back(t == infinity ? -1 : t);
    }
  }
  return ans;
}

int main() {
  string in =
      R"(1
94 95
1 19 56
2 57 41
3 1 32
4 1 36
5 1 24
6 19 16
7 19 25
8 76 6
9 76 23
10 19 25
11 76 64
12 19 6
13 76 12
14 76 18
15 76 47
16 1 53
17 19 36
18 19 25
19 38 55
20 1 1
21 76 57
22 1 44
23 57 19
24 38 15
25 1 16
26 19 37
27 76 22
28 57 54
29 19 47
30 76 15
31 57 5
32 19 13
33 19 65
34 1 19
35 19 17
36 38 12
37 1 5
38 19 23
39 76 33
40 38 19
41 19 4
42 76 32
43 57 27
44 57 18
45 1 13
46 19 40
47 1 62
48 76 46
49 1 18
50 19 4
51 1 58
52 1 9
53 19 5
54 57 63
55 76 29
56 1 28
57 38 26
58 19 47
59 57 43
60 76 37
61 76 14
62 1 40
63 76 27
64 1 28
65 76 32
66 1 47
67 38 21
68 76 58
69 38 10
70 38 46
71 57 59
72 1 7
73 38 55
74 57 51
75 38 45
76 77 9
77 76 46
78 38 43
79 1 61
80 57 43
81 1 2
82 19 64
83 1 9
84 1 14
85 57 49
86 57 22
87 19 12
88 76 55
89 76 26
90 1 36
91 1 63
92 57 51
93 19 7
94 1 24
55 76 9
85)";
  stringstream ss(in);
  auto &cin = ss;
  auto &fout = cout;
  //  ofstream fout(getenv("OUTPUT_PATH"));
  std::ios_base::sync_with_stdio(false);
  int t;
  cin >> t;
  cin.ignore(numeric_limits<streamsize>::max(), '\n');

  for (int t_itr = 0; t_itr < t; t_itr++) {
    string nm_temp;
    getline(cin, nm_temp);

    vector<string> nm = split_string(nm_temp);

    int n = stoi(nm[0]);

    int m = stoi(nm[1]);

    vector<vector<int>> edges(m);
    for (int i = 0; i < m; i++) {
      edges[i].resize(3);

      for (int j = 0; j < 3; j++) {
        cin >> edges[i][j];
      }

      cin.ignore(numeric_limits<streamsize>::max(), '\n');
    }

    int s;
    cin >> s;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    vector<int> result = shortestReach(n, edges, s);

    for (int i = 0; i < result.size(); i++) {
      fout << result[i];

      if (i != result.size() - 1) {
        fout << " ";
      }
    }

    fout << "\n";
  }

  //  fout.close();

  return 0;
}

vector<string> split_string(string input_string) {
  string::iterator new_end =
      unique(input_string.begin(), input_string.end(),
             [](const char &x, const char &y) { return x == y and x == ' '; });

  input_string.erase(new_end, input_string.end());

  while (input_string[input_string.length() - 1] == ' ') {
    input_string.pop_back();
  }

  vector<string> splits;
  char delimiter = ' ';

  size_t i = 0;
  size_t pos = input_string.find(delimiter);

  while (pos != string::npos) {
    splits.push_back(input_string.substr(i, pos - i));

    i = pos + 1;
    pos = input_string.find(delimiter, i);
  }

  splits.push_back(
      input_string.substr(i, min(pos, input_string.length()) - i + 1));

  return splits;
}
