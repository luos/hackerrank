/// Hash Tables: Ice Cream Parlor
/// https://www.hackerrank.com/challenges/ctci-ice-cream-parlor/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=search

#include <bits/stdc++.h>

using namespace std;

vector<string> split_string(string);

// Complete the whatFlavors function below.
void whatFlavors(vector<int> cost, int money) {
  unordered_multimap<int, int> map;
  for (int i = 0; i < cost.size(); i++) {
    if (i < money) {
      map.emplace(cost[i], i + 1);
    }
  }
  for (int i = 0; i < cost.size(); i++) {
    auto a = cost[i];
    auto b = money - a;
    auto it = map.find(b);
    if (it != map.end()) {
      if (a != b) {
        cout << i + 1 << " " << it->second << endl;
        return;
      } else {
        auto r = map.equal_range(b);
        vector<int> result;
        for (auto it = r.first; it != r.second; it++) {
          result.push_back(it->second);
        }
        if (result.size() > 1) {
          auto a = result.front();
          auto b = result.back();
          cout << min(a, b) << " " << max(a, b) << endl;
          return;
        }
      }
    }
  }
}

int main() {
  string in =
      R"(2
4
5
1 4 5 3 2
4
4
2 2 4 3)";
  stringstream ss(in);
  auto &cin = ss;
  int t;
  cin >> t;
  cin.ignore(numeric_limits<streamsize>::max(), '\n');

  for (int t_itr = 0; t_itr < t; t_itr++) {
    int money;
    cin >> money;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    int n;
    cin >> n;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    string cost_temp_temp;
    getline(cin, cost_temp_temp);

    vector<string> cost_temp = split_string(cost_temp_temp);

    vector<int> cost(n);

    for (int i = 0; i < n; i++) {
      int cost_item = stoi(cost_temp[i]);

      cost[i] = cost_item;
    }

    whatFlavors(cost, money);
  }

  return 0;
}

vector<string> split_string(string input_string) {
  string::iterator new_end =
      unique(input_string.begin(), input_string.end(),
             [](const char &x, const char &y) { return x == y and x == ' '; });

  input_string.erase(new_end, input_string.end());

  while (input_string[input_string.length() - 1] == ' ') {
    input_string.pop_back();
  }

  vector<string> splits;
  char delimiter = ' ';

  size_t i = 0;
  size_t pos = input_string.find(delimiter);

  while (pos != string::npos) {
    splits.push_back(input_string.substr(i, pos - i));

    i = pos + 1;
    pos = input_string.find(delimiter, i);
  }

  splits.push_back(
      input_string.substr(i, min(pos, input_string.length()) - i + 1));

  return splits;
}
