/// DFS: Connected Cell in a Grid
/// https://www.hackerrank.com/challenges/ctci-connected-cell-in-a-grid/problem?h_l=interview&playlist_slugs%5B%5D%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D%5B%5D=graphs

#include <bits/stdc++.h>

using namespace std;

int region(int r, int c, vector<vector<int>> &grid, vector<vector<int>> &label,
           int tag) {
  int dx[] = {0, 0, 1, -1, 1, 1, -1, -1};
  int dy[] = {1, -1, 0, 0, 1, -1, 1, -1};
  const int N = 8;
  int count = 1;
  label[r][c] = tag;
  auto n = grid.size();
  auto m = grid.front().size();

  for (int k = 0; k < N; k++) {
    auto i = r + dy[k];
    auto j = c + dx[k];
    if (i >= 0 && i < n && j >= 0 && j < m && grid[i][j] == 1 &&
        label[i][j] == 0) {
      count += region(i, j, grid, label, tag);
    }
  }
  return count;
}

// Complete the maxRegion function below.
int maxRegion(vector<vector<int>> &grid) {
  if (grid.empty()) {
    return 0;
  }
  auto n = grid.size();
  auto m = grid.front().size();
  vector<vector<int>> label(grid.size());
  for (auto &i : label) {
    i.resize(m);
  }

  int result = 0;
  int tag = 0;
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < m; j++) {
      if (grid[i][j] == 1 && label[i][j] == 0) {
        auto r = region(i, j, grid, label, ++tag);
        result = r > result ? r : result;
      }
    }
  }
  return result;
}

int main() {
  string in =
      R"(4
4
1 1 0 0
0 1 1 0
0 0 1 0
1 0 0 0)";
  stringstream ss(in);
  auto &cin = ss;
  //  ofstream fout(getenv("OUTPUT_PATH"));
  auto &fout = cout;

  int n;
  cin >> n;
  cin.ignore(numeric_limits<streamsize>::max(), '\n');

  int m;
  cin >> m;
  cin.ignore(numeric_limits<streamsize>::max(), '\n');

  vector<vector<int>> grid(n);
  for (int i = 0; i < n; i++) {
    grid[i].resize(m);

    for (int j = 0; j < m; j++) {
      cin >> grid[i][j];
    }

    cin.ignore(numeric_limits<streamsize>::max(), '\n');
  }

  int res = maxRegion(grid);

  fout << res << "\n";

  //  fout.close();

  return 0;
}
