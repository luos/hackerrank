/// BFS: Shortest Reach in a Graph
/// https://www.hackerrank.com/challenges/ctci-bfs-shortest-reach/problem?h_l=interview&playlist_slugs%5B%5D%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D%5B%5D=graphs

#include <algorithm>
#include <cmath>
#include <cstdio>
#include <iostream>
#include <queue>
#include <sstream>
#include <unordered_map>
#include <vector>
using namespace std;

class Graph {
  int n;
  unordered_multimap<int, int> adjacency;
  unordered_map<int, int> distances;

public:
  Graph(int n) {
    this->n = n;
    for (int i = 1; i <= n; i++) {
      distances[i] = -1;
    }
  }

  void add_edge(int u, int v) {
    adjacency.emplace(u, v);
    adjacency.emplace(v, u);
  }

  vector<int> shortest_reach(int start) {
    auto const &s = start;
    queue<int> vertices;
    vertices.push(s);
    distances[s] = 0;
    while (!vertices.empty()) {
      auto i = vertices.front();
      vertices.pop();
      auto range = adjacency.equal_range(i);
      for (auto it = range.first; it != range.second; ++it) {
        auto j = it->second;
        if (-1 == distances[j]) {
          vertices.push(j);
          distances[j] = distances[i] + 6;
        }
      }
    }
    vector<int> result;
    result.reserve(n - 1);
    for (int i = 1; i <= n; i++) {
      if (i != s) {
        result.push_back(distances[i]);
      }
    }
    return result;
  }
};

int main() {
  string input =
      R"(2
4 2
1 2
1 3
1
3 1
2 3
2)";

  stringstream ss(input);
  auto &cin = ss;

  int queries;
  cin >> queries;

  for (int t = 0; t < queries; t++) {

    int n, m;
    cin >> n;
    // Create a graph of size n where each edge weight is 6:
    Graph graph(n);
    cin >> m;
    // read and set edges
    for (int i = 0; i < m; i++) {
      int u, v;
      cin >> u >> v;
      //      u--, v--;
      // add each edge to the graph
      graph.add_edge(u, v);
    }
    int startId;
    cin >> startId;
    //    startId--;
    // Find shortest reach from node s
    vector<int> distances = graph.shortest_reach(startId);

    for (int i = 0; i < distances.size(); i++) {
      //      if (i != startId) {
      cout << distances[i] << " ";
      //      }
    }
    cout << endl;
  }

  return 0;
}
