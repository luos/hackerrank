;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; simulate input from STDIN for sample test cases
(defn input [text] (def line (seq (clojure.string/split-lines text))))

(defn print-input [in]
  (when-let [l (in)]
    (do
      (prn l)
      (recur in))))

(defn read-line []
  (when-let [l (first line)]
    (do
      (def line (next line))
      l)))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(println "\nRamanujan’s Prime Substrings")

(input
 "1
1333657")

(def max-len 6)

(defn trimsubs [s]
  (map #(subs s 0 (inc %)) (range (min max-len (count s)))))

(defn getsubs [s]
  (let [ss (map #(subs s % (min (+ % max-len) (count s))) (range (count s)))]
    (reduce concat (map trimsubs ss))))

(defn prime-numbers [num]
  (loop [p [2]
         n (range 3 (inc num) 2)]
    (if (or (empty? n) (> (first n) (Math/sqrt (last n))))
      (concat p n)
      (recur (conj p (first n)) (filter #(not= 0 (mod % (first n))) n)))))

(defn main []
  (let [n (read-string (read-line))
        v (map (fn [_] (read-line)) (range n))]
    (doseq [i v]
      (let [ss (sort (distinct (map read-string (getsubs i))))
            ps (set (prime-numbers (last ss)))
            common (filter #(contains? ps %) ss)]
        (prn (count common))))))

(time (main))
