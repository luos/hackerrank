;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; simulate input from STDIN for sample test cases
(defn input [text] (def line (seq (clojure.string/split-lines text))))

(defn print-input [in]
  (when-let [l (in)]
    (do
      (prn l)
      (recur in))))

(defn read-line []
  (when-let [l (first line)]
    (do
      (def line (next line))
      l)))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(println "\nPascal's Triangle")
(input "4")

(let [n (read-string (read-line))
      i '(1)]
  (loop [a i]
    (apply prn a)
    (when (< (count a) n)
      (recur (concat i (map + a (next a)) i)))))

(println "\nBreadth First Search: Shortest Reach")
(input
 "2
5 4
1 2
1 3
2 3
2 4
1
3 1
2 3
2")

; Complete the bfs function below.
(defn bfs [n m edges s]
  (let [r (range 1 (inc n))
        e (map set edges)
        adj (zipmap r (for [i r] (disj (reduce into #{} (filter #(% i) e)) i)))]
    (loop [v (list s)
           dist (zipmap r (for [i r] (if (= i s) 0 nil)))]
      (let [a (first v)
            neighbors (remove dist (adj a))
            q (concat (next v) neighbors)]
        (if (empty? q)
          (map #(if (nil? %) -1 %) (map dist (remove #(= s %) r)))
          (recur q (merge dist (zipmap neighbors (repeat (+ 6 (dist a)))))))))))

(def q (Integer/parseInt (clojure.string/trim (read-line))))
(doseq [q-itr (range q)]
  (def nm (clojure.string/split (read-line) #" "))
  (def n (Integer/parseInt (clojure.string/trim (nth nm 0))))
  (def m (Integer/parseInt (clojure.string/trim (nth nm 1))))
  (def edges [])
  (doseq [_ (range m)]
    (def edges (conj edges (vec (map #(Integer/parseInt %) (clojure.string/split (read-line) #" "))))))
  (def s (Integer/parseInt (clojure.string/trim (read-line))))
  (def result (bfs n m edges s))
  (apply prn result))
