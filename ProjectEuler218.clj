;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; simulate input from STDIN for sample test cases
(defn input [text] (def line (seq (clojure.string/split-lines text))))

(defn print-input [in]
  (when-let [l (in)]
    (do
      (prn l)
      (recur in))))

(defn read-line []
  (when-let [l (first line)]
    (do
      (def line (next line))
      l)))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(println "\nProject Euler #218: Perfect right-angled triangles")

(input
 "1
25")

(defn long? [n]
  (= n (double (long n))))

(defn gcd [m n]
  (if (zero? n)
    m
    (recur n (mod m n))))

(defn triangle [c]
  (let [amax (long (Math/sqrt (/ (Math/pow c 2) 2)))]
    (for [a (range 1 (inc amax))
          :let [b (Math/sqrt (- (Math/pow c 2) (Math/pow a 2)))
                bl (long b)]]
      (if (and (long? b) (= 1 (gcd bl a)) (= 1 (gcd c bl)))
        [a bl c]
        nil))))

(defn square? [n]
  (= n (long (Math/pow (long (Math/sqrt n)) 2))))

(defn perfect-square [n]
  (filter square? (range 25 (inc n))))

(defn super-perfect? [abc]
  (let [area (/ (* (first abc) (second abc)) 2)]
    (and (not= 1 (gcd area 6)) (not= 1 (gcd area 28)))))

(defn main []
  (let [n (read-string (read-line))
        v (map (fn [_] (read-string (read-line))) (range n))]
    (doseq [i v
            :let [cs (perfect-square i)
                  triangles (remove nil? (reduce concat (map triangle cs)))
                  non-perfect (remove super-perfect? triangles)]]
      (prn (count non-perfect)))))

(time (main))

(input
 "2
25
97")

(let [n (read-string (read-line))
      v (map (fn [_] (read-string (read-line))) (range n))]
  (doseq [i v]
    (prn i)))
