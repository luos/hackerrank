;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; simulate input from STDIN for sample test cases
(defn input [text] (def line (seq (clojure.string/split-lines text))))

(defn print-input [in]
  (when-let [l (in)]
    (do
      (prn l)
      (recur in))))

(defn read-line []
  (when-let [l (first line)]
    (do
      (def line (next line))
      l)))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(input (slurp "input01.txt"))

(defn count-input [in i n]
  (when-let [l (in)]
    (do
      (let [a (read-string (str "[" l "]"))]
        (if (< (count a) 3)
          (prn n i a))
        (recur in (inc i) (if (= 1 (count a)) (inc n) n)))
)))

(count-input read-line 1 0)
